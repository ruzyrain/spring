$('#editform').submit(function(){

    var formData = new FormData($('#editform')[0]);
    // process the form
    $.ajax({
        type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
        url         : '/user/editavatar.json', // the url where we want to POST
        data        : formData, // our data object
        contentType : false,
        processData : false,
        dataType    : 'json' // what type of data do we expect back from the server
    })
        // using the done promise callback
        .done(function(data) {
            if(data.code == 0){
                //fail
                window.location.reload();
            }else{
                window.location.href = "/home";
            }

        });

    // stop the form from submitting the normal way and refreshing the page
    event.preventDefault();

});

function getFileUrl(sourceId) {

    var url;
    if (navigator.userAgent.indexOf("MSIE")>=1) { // IE
        url = document.getElementById(sourceId).value;
    } else if(navigator.userAgent.indexOf("Firefox")>0) { // Firefox
        url = window.URL.createObjectURL(document.getElementById(sourceId).files.item(0));
    } else if(navigator.userAgent.indexOf("Chrome")>0) { // Chrome
        url = window.URL.createObjectURL(document.getElementById(sourceId).files.item(0));
    }
    return url;
}

/**
 * 将本地图片 显示到浏览器上
 */
function preImg(sourceId, targetId) {
    var url = getFileUrl(sourceId);
    var imgPre = document.getElementById(targetId);
    imgPre.src = url;
}




