$(function(){
    var node = $("#commenthidden")
    node.hide();

    $(".add").bind("click",function(){

        if(!node.is(':visible')){
            node.show();
        }else {
            node.hide();
        }
    })

    $('.commentform').submit(function(){

        var formData = new FormData($('.commentform')[0]);
        // process the form
        $.ajax({
            type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url         : '/home/commentadd.json', // the url where we want to POST
            data        : formData, // our data object
            contentType : false,
            processData : false,
            dataType    : 'json' // what type of data do we expect back from the server
        })
            // using the done promise callback
            .done(function(data) {
                if(data.code == 0){
                    //fail
                    window.location.reload();
                }else{
                    window.location.reload();
                }

            });

        // stop the form from submitting the normal way and refreshing the page
        event.preventDefault();

    });

});