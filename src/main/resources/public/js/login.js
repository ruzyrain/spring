$(function(){
    /*
        Background slideshow
    */
    $.backstretch([
      "/img/login/1.jpg"
    , "/img/login/2.jpg"
    , "/img/login/3.jpg"
    ], {duration: 3000, fade: 750});

    /*
        Tooltips
    */
    $('.links a.home').tooltip();
    $('.links a.blog').tooltip();


    /*
     Login Form validation
     */
    $('#loginform').submit(function(){
        $(this).find("label[for='username']").html('Username');
        $(this).find("label[for='password']").html('Password');
        ////
        var username = $(this).find('input#username').val();
        var password = $(this).find('input#password').val();
        if(username == '') {
            $(this).find("label[for='username']").append("<span style='display:none' class='red'> - Please enter a valid username.</span>");
            $(this).find("label[for='username'] span").fadeIn('medium');
            return false;
        }
        if(password == '') {
            $(this).find("label[for='password']").append("<span style='display:none' class='red'> - Please enter a valid password.</span>");
            $(this).find("label[for='password'] span").fadeIn('medium');
            return false;
        }


        // process the form
        $.ajax({
            type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url         : '/user/login.json', // the url where we want to POST
            data        : $('#loginform').serialize(), // our data object
            processData : false,
            dataType    : 'json' // what type of data do we expect back from the server
        })
            // using the done promise callback
            .done(function(data) {
                if(data.code == 0){
                    //fail
                    $("#loginform").find("label[for='username']").append("<span style='display:none' class='red'> - " + data.reason + "</span>");
                    $("#loginform").find("label[for='username'] span").fadeIn('medium');
                }else{
                    window.location.href = "/home";
                }

            });

        // stop the form from submitting the normal way and refreshing the page
        event.preventDefault();

    });

    /*
        Regist Form validation
    */
    $('#registform').submit(function(){
        $(this).find("label[for='username']").html('Username');
        $(this).find("label[for='email']").html('Email');
        $(this).find("label[for='password']").html('Password');
        ////
        var username = $(this).find('input#username').val();
        var email = $(this).find('input#email').val();
        var password = $(this).find('input#password').val();

        if(username == '') {
            $(this).find("label[for='username']").append("<span style='display:none' class='red'> - Please enter a valid username.</span>");
            $(this).find("label[for='username'] span").fadeIn('medium');
            return false;
        }
        if(email == '') {
            $(this).find("label[for='email']").append("<span style='display:none' class='red'> - Please enter a valid email.</span>");
            $(this).find("label[for='email'] span").fadeIn('medium');
            return false;
        }
        if(password == '') {
            $(this).find("label[for='password']").append("<span style='display:none' class='red'> - Please enter a valid password.</span>");
            $(this).find("label[for='password'] span").fadeIn('medium');
            return false;
        }

        var formData = new FormData($('#registform')[0]);
        // process the form
        $.ajax({
            type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url         : '/user/regist.json', // the url where we want to POST
            data        : formData , // our data object
            contentType : false,
            processData : false,
            dataType    : 'json' // what type of data do we expect back from the server
        })
            // using the done promise callback
            .done(function(data) {
                if(data.code == 0){
                    //fail
                    alert(data.code);
                    $("#registform").find("label[for='username']").append("<span style='display:none' class='red'> - " + data.reason + "</span>");
                    $("#registform").find("label[for='username'] span").fadeIn('medium');
                }else{
                    window.location.href = "/home";
                }

            });

        // stop the form from submitting the normal way and refreshing the page
        event.preventDefault();
    });

    //original show
    $("#loginform").show();
    $("#registform").hide();

    //form switch
    $(".log").bind("click",function(){
        $(".log").addClass("red");
        $(".reg").removeClass("red");
        $("#loginform").show();
        $("#registform").hide();

    })
    $(".reg").bind("click",function(){
        $(".reg").addClass("red");
        $(".log").removeClass("red");
        $("#loginform").hide();
        $("#registform").show();
    })

});


