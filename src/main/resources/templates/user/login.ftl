<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <title>Login Page</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- CSS -->
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=PT+Sans:400,700'>
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Oleo+Script:400,700'>
    <link rel="stylesheet" href="/css/lib/bootstrap.min.css">
    <link rel="stylesheet" href="/css/login.css">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

</head>

<body>

<div class="header">
    <div class="container">
        <div class="row">
            <div class="logo span4">
                <h1><a href="">TaoBlog Register <span class="red">.</span></a></h1>
            </div>
            <div class="links span8">
                <a class="home" href="" rel="tooltip" data-placement="bottom" data-original-title="Home"></a>
                <a class="blog" href="" rel="tooltip" data-placement="bottom" data-original-title="Blog"></a>
            </div>
        </div>
    </div>
</div>

<div class="register-container container">
    <div class="row">
        <div class="iphone span5">
            <img src="/img/login/iphone.png" alt="">
        </div>
        <div class="register span6">

            <form action="" method="post" id="loginform">
                <h2>
                    <span class="log red hov"><strong>Login </strong></span>
                    <span class="reg hov"><strong> REGIST</strong></span>
                </h2>
                <label for="username">Username</label>
                <input type="text" id="username" name="username" placeholder="choose a username...">

                <label for="password">Password</label>
                <input type="password" id="password" name="password" autocomplete="off" placeholder="choose a password...">
                <button type="submit">LOGIN</button>
            </form>

            <form action="" method="post" id="registform">
                <h2>
                    <span class="log red hov"><strong>Login </strong></span>
                    <span class="reg hov"><strong> REGIST</strong></span>
                </h2>
                <label for="username">Username</label>
                <input type="text" id="username" name="username" autocomplete="off" placeholder="choose a username...">

                <label for="email">Email</label>
                <input type="text" id="email" name="email" autocomplete="off" placeholder="enter your email...">

                <label for="password">Password</label>
                <input type="password" id="password" name="password" autocomplete="off" placeholder="choose a password...">

                <label for="avatar">Avatar</label>
                <input type="file" name="avatar" autocomplete="off" id="avatar"/>

                <button type="submit">REGISTER</button>
            </form>
        </div>
    </div>
</div>
<div align="center">Copyright &copy; 2016.Tao Ye All rights reserved.</div>

<!-- Javascript -->
<script src="/js/lib/jquery-1.8.2.min.js"></script>
<script src="/js/lib/bootstrap.min.js"></script>
<script src="/js/lib/jquery.backstretch.min.js"></script>
<script src="/js/login.js"></script>


</body>
</html>