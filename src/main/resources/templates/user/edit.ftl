<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <title>Edit Page</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- CSS -->
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=PT+Sans:400,700'>
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Oleo+Script:400,700'>
    <link rel="stylesheet" href="/css/lib/bootstrap.min.css">
    <link rel="stylesheet" href="/css/login.css">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

</head>

<body>
    <div class="span3">
        <#include "../common/left.ftl" />
    </div>
    <div class="span9">
        <form action="" method="post" id="editform">
            <label for="avatar">Avatar</label>
            <input type="file" name="avatar" autocomplete="off" id="avatar" onchange="preImg(this.id,'img')" />

            <img id="img" style="display: block;" />

            <button type="submit">SURE</button>
        </form>
    </div>

<!-- Javascript -->
<script src="/js/lib/jquery-1.8.2.min.js"></script>
<script src="/js/edit.js"></script>



</body>
</html>