<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <title>Home Page</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- CSS -->
    <link rel="stylesheet" href="/css/lib/bootstrap.min.css">
    <link rel="stylesheet" href="/css/home.css">

</head>

<body>
    <div class="span3">
        <#include "../common/left.ftl" />
    </div>
    <div class="span9">
        <span>
            <img class="useravatar" src="${user.image!}"  onerror="this.onerror=null;this.src='/img/default/avatar.jpg/'"/>
            欢迎 <a href="/user/edit"> ${user.userName!} </a> 登录！！
        </span>

        <span>

        </span>

        <span>
            <ul id="article" class="list">
            </ul>
        </span>

        <button class="more">more</button>
        <button class="add">add</button>
        <br /><br /><br />
        <div class="" id="articlehidden">

            <form class="atricleform" method="post">
                <input type="text" name="title" placeholder="标题"/>
                <input type="file" name="image" autocomplete="off"/>
                <input type="text" name="content" placeholder="内容" />
                <button type="submit">SURE</button>
            </form>

        </div>
    </div>


    <!-- Javascript -->
    <script src="/js/lib/jquery-1.8.2.min.js"></script>
    <script src="/js/home.js"></script>
    <script src="/js/lib/bootstrap.min.js"></script>


</body>
</html>