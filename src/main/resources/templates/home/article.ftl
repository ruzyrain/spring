<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <title>Article Info</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- CSS -->
    <link rel="stylesheet" href="/css/lib/bootstrap.min.css">
    <link rel="stylesheet" href="/css/article.css">

</head>

<body>
    <div class="span3">
        <#include "../common/left.ftl" />
    </div>
    <div class="span9">
        <span>
        ${article.title!}

        <#--<img src="${article.image!}" />-->
        ${article.time!}


        <#list commentlist! as co>
            <li>${co.user.userName!} 于 ${co.time!} 发表了评论: </li>
                <li class="li_style"> <img class="commentimage" src="${co.image}" />
                ${co.content!}</li>
        </#list>
    </span>

        <br />
    <span>
        <a href="/home"><button>返回</button></a>
    </span>


        <button class="more"> more </button>

        <button class="add">add</button>
        <br /><br /><br />

        <div class="" id="commenthidden">

            <form class="commentform" method="post">

                <input type="hidden" name="articleid" value="${article.id!}">

                <input type="text" name="content" placeholder="内容" />
                <button type="submit">SURE</button>
            </form>

        </div>
    </div>




    <!-- Javascript -->
    <script src="/js/lib/jquery-1.8.2.min.js"></script>
    <script src="/js/article.js"></script>


</body>
</html>