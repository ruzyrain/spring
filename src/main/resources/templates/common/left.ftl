<ul class="nav nav-tabs nav-stacked">
    <li class="nav-header active">主导航</li>
    <li><a href="/home">首页</a></li>
</ul>

<div class="panel-group" id="accordion2">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion2"
                   href="#collapseOne">
                    <i class="icon-bar-chart"></i>  文章列表
                </a>
            </h4>
        </div>
        <div id="collapseOne" class="panel-collapse collapse in">
            <div class="panel-body">
                <ul class="nav nav-list">
                    <li class=""><a href="myresources/deploy">部署资源</a></li>
                    <li><a href="myresources/all">我的所有资源</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<ul class="nav nav-tabs nav-stacked">
    <li><a href="/user/logout.jsonr">登出</a></li>
</ul>
