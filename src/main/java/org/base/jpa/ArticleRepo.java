package org.base.jpa;

import org.base.entity.Article;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;


/**
 * Created by taoye on 16/1/27.
 */
public interface ArticleRepo extends PagingAndSortingRepository<Article,Long> {


    public Article findOneById(Long id);

    Page<Article> findByUserId(Long userId, Pageable pageable);
}
