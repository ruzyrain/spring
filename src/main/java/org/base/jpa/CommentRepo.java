package org.base.jpa;

import org.base.entity.Comment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Created by taoye on 16/1/27.
 */
public interface CommentRepo extends PagingAndSortingRepository<Comment,String> {

    Page<Comment> findByArticleId(Long articleId, Pageable pageable);

}
