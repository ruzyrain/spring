package org.base.jpa;

import org.base.entity.User;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

/**
 * Created by taoye on 16/1/27.
 */
public interface UserRepo extends PagingAndSortingRepository<User,Long> {

    public User findByUserNameAndPassword(String userName, String password);

    public List<User> findByUserName(String userName);
}
