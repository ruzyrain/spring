package org.base.config;

import org.base.interceptor.LoginStatusInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Created by taoye on 16/1/27.
 */
@Configuration
public class InterceptorConfig extends WebMvcConfigurerAdapter{

    @Override
    public void addInterceptors(InterceptorRegistry registry){

        registry.addInterceptor(new LoginStatusInterceptor()).addPathPatterns("/**");

        super.addInterceptors(registry);
    }

//    @Override
//    public void addResourceHandlers(ResourceHandlerRegistry registry){
//
//        registry.addResourceHandler("page","css","js","public.img");
//        super.addResourceHandlers(registry);
//
//    }
}
