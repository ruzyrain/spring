package org.base.entity.Pojo;

import org.base.entity.User;

/**
 * Created by taoye on 16/2/22.
 */
public class UserMyselfVo {

    private Long id;

    private String userName;
    private String image;

    private UserMyselfVo(){

    }

    public UserMyselfVo(User user){

        this.id = user.getId();
        this.userName = user.getUserName();
        this.image = user.getImage();
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
