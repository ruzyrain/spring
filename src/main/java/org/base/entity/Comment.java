package org.base.entity;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by taoye on 16/2/22.
 */

@Entity
@Table(name = "comment")
public class Comment {

    @Id
    private String id;

    private String image;
    private String content;
    private Date time;

    @ManyToOne
    private User user;

    @ManyToOne
    private Article article;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
