package org.base.util;

import org.apache.log4j.Logger;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Random;

/**
 * Created by taoye on 16/1/28.
 */
public class Utility {
    private static final Logger logger = Logger.getLogger(Utility.class);

    /**
     * 产生随机文件名
     */
    private static String RandomSeed = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ_-+=";
    public static int RandomLen = 30;
    public static String random(){
        StringBuilder sb = new StringBuilder();
        Random rd = new Random();
        for (int i = 0; i < RandomLen; i++){
            sb.append(RandomSeed.charAt(rd.nextInt(RandomSeed.length())));
        }
        return sb.toString();
    }


    /**
     * 服务器所有图片统一存放在./public/img文件夹下，path为文件夹下的类型，比如avatar、res等
     * @param path
     * @param file
     * @return
     */
    public static String saveFile(String path, MultipartFile file) {

        //可以专门采用文件服务器处理，这里针对的是mac系统，其他系统不一定通用
        String[] p = Utility.class.getResource("/public").toString().split(":");
        String rootPath = p[p.length - 1];

        String fileName;
        File localFile;

        do {
            fileName = random() + ".png";
            logger.info("current path is " + rootPath);
            localFile = new File(rootPath + "/img/" + path + "/" + fileName);
        }while (localFile.exists());

        String realPath = rootPath + "/img/" + path + "/" + fileName;

        // Save file
        if (!file.isEmpty()) {
            File f = new File(realPath);
            if (!f.getParentFile().exists()) {
                f.getParentFile().mkdirs();
            } else if (!f.getParentFile().isDirectory()) {
                logger.debug("not a directory.");
                return "";
            }

            try {
//                BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(f));
//                byte[] bytes = file.getBytes();
//                System.err.println("byte size is " + bytes.length);
//                stream.write(bytes);
                converter(file.getInputStream(),"png",f);

            } catch (IOException e) {
                e.printStackTrace();
            }

        } else {
            logger.debug("File cannot be empty.");
            return "";
        }
        return "/img/" + path + "/" + fileName;
    }

    /**
     * to remove the file
     * @param filePathAndName
     * @return
     */
    public static boolean removeFile(String filePathAndName) {

        String[] p = Utility.class.getResource("/public").toString().split(":");
        String rootPath = p[p.length - 1];

        String realPath = rootPath + filePathAndName;

        File oldFile = new File(realPath);

        try{
            oldFile.delete();
        }catch (Exception e){
            return false;
        }

        return true;
    }

    public static void converter(InputStream imgFile,String format,File formatFile)
            throws IOException{

        BufferedImage bIMG = ImageIO.read(imgFile);
        ImageIO.write(bIMG, format, formatFile);

        bIMG.flush();

    }
}

