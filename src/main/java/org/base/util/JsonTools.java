package org.base.util;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.base.entity.Article;
import org.base.entity.Comment;
import org.base.entity.Pojo.UserMyselfVo;
import org.base.entity.Pojo.UserOtherVo;
import org.base.entity.User;
import org.springframework.data.domain.Page;

import java.text.SimpleDateFormat;
import java.util.Iterator;

/**
 * Created by taoye on 16/2/22.
 */
public class JsonTools {

    //User
    public static JSONObject userMyself(User myself) {

        JSONObject reJson = JSONObject.fromObject(new UserMyselfVo(myself));

        return reJson;
    }

    public static JSONObject userOther(User other) {

        JSONObject reJson = JSONObject.fromObject(new UserOtherVo(other));

        return reJson;
    }

    public static JSONArray userOtherList(Page<User> userOtherList){

        JSONArray reJson = new JSONArray();

        Iterator it = userOtherList.iterator();

        while (it.hasNext()){
            reJson.add(userOther((User)it.next()));
        }
        return reJson;
    }

    //Article
    public static JSONObject articleAbstract(Article article){

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        JSONObject reJson = new JSONObject();
        reJson.put("id", article.getId());
        reJson.put("title", article.getTitle());
        reJson.put("image", article.getImage());
        reJson.put("time", sdf.format(article.getTime()));
        reJson.put("user", new UserMyselfVo(article.getUser()));

        return reJson;
    }

    public static JSONObject articleDetail(Article article) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        JSONObject reJson = new JSONObject();

        reJson.put("id", article.getId());
        reJson.put("title", article.getTitle());
        reJson.put("content", article.getContent());
        reJson.put("image", article.getImage());
        reJson.put("time", sdf.format(article.getTime()));
        reJson.put("user", new UserMyselfVo(article.getUser()));

        return reJson;
    }

    public static JSONArray articleList(Page<Article> articleList){

        JSONArray reJson = new JSONArray();

        Iterator it = articleList.iterator();

        while (it.hasNext()){
            reJson.add(articleAbstract((Article) it.next()));
        }
        return reJson;
    }

    //Comment

    public static JSONObject commentInfo(Comment comment){

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        JSONObject reJson = new JSONObject();
        reJson.put("id", comment.getId());
        reJson.put("articleid", comment.getArticle().getId());
        reJson.put("content", comment.getContent());
        reJson.put("image", comment.getImage());
        reJson.put("time", sdf.format(comment.getTime()));
        reJson.put("user", new UserOtherVo(comment.getUser()));

        return reJson;
    }

    public static JSONArray commentList(Page<Comment> commentList){

        JSONArray reJson = new JSONArray();

        Iterator it = commentList.iterator();

        while (it.hasNext()){
            reJson.add(commentInfo((Comment) it.next()));
        }
        return reJson;
    }

}
