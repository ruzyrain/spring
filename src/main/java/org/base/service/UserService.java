package org.base.service;

import org.base.entity.User;
import org.base.jpa.UserRepo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by taoye on 16/1/27.
 */
@Service
public class UserService {

    @Resource
    UserRepo userRepo;

    public User getUserByNameAndPass(String userName, String password){

        return userRepo.findByUserNameAndPassword(userName,password);
    }

    public User save(User user){

        return userRepo.save(user);
    }

    public Boolean getByUserName(String userName){
        return userRepo.findByUserName(userName).size() > 0? true:false;
    }
}
