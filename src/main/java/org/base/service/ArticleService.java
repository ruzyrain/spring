package org.base.service;

import org.base.entity.Article;
import org.base.jpa.ArticleRepo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by taoye on 16/1/27.
 */
@Service
public class ArticleService {

    @Resource
    ArticleRepo articleRepo;

    public Page<Article> getAllByTimeAndPage(Long userId, int page){

        Sort sort = new Sort(Sort.Direction.DESC, "time");

        PageRequest pageable = new PageRequest(page, 10, sort);

        return articleRepo.findByUserId(userId, pageable);
    }

    public Article save(Article article){

        return articleRepo.save(article);
    }

    public Article getById(Long id){

        return articleRepo.findOneById(id);
    }

}
