package org.base.service;

import org.base.entity.Comment;
import org.base.jpa.CommentRepo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by taoye on 16/1/27.
 */
@Service
public class CommentService {

    @Resource
    CommentRepo commentRepo;

    public Page<Comment> getAllByArticleAndPage(Long articleId, int page){

        Sort sort = new Sort(Sort.Direction.DESC, "time");
        PageRequest pageable = new PageRequest(page, 10, sort);

        return commentRepo.findByArticleId(articleId,pageable);
    }

    public Comment save(Comment comment){

        return commentRepo.save(comment);
    }

}
