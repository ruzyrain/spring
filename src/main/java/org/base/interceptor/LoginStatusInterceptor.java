package org.base.interceptor;

import net.sf.json.JSONObject;
import org.apache.log4j.Logger;
import org.aspectj.weaver.ast.Not;
import org.base.controller.BaseJsonJumpAction;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.util.Map;

/**
 * Created by taoye on 16/1/27.
 */
public class LoginStatusInterceptor extends BaseJsonJumpAction implements HandlerInterceptor{

    private Logger logger = Logger.getLogger(this.getClass());

    public static int getNum=0;

    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response, Object handler) throws Exception {

        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");


        logger.info("网站访问次数: " + (++LoginStatusInterceptor.getNum));
        logger.info("请求的网址是: " + request.getRequestURL());
        Map<String, String[]> para = request.getParameterMap();
        for (Map.Entry<String, String[]> entry : para.entrySet()) {
            logger.info("key= " + entry.getKey() + " and value= " + entry.getValue()[0]);
        }

        // 设置不阻拦的path
        String[] noFilters = new String[] {"/user/login","/user/regist"
        };

        String uri = request.getRequestURI();

//        //处理对网站根路径的访问
//        if(uri.equals("/yuedao/"))
//        {BaseJsonJumpAction.java
//            return super.preHandle(request, response, handler);
//        }

        boolean beFilter = true;
        for (String s : noFilters) {
            if (uri.indexOf(s) != -1) {
                beFilter = false;
                break;
            }
        }
        if (beFilter) {
            Object obj = request.getSession().getAttribute("user");

            //未登录
            if (null == obj) {
//                PrintWriter out = response.getWriter();
//                StringBuilder builder = new StringBuilder();
//                //not log in
//                builder.append(NotLogin());
//                out.print(builder.toString());
//                out.close();
                response.sendRedirect("/user/login");
                return false;
            }
        }

        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }
}
