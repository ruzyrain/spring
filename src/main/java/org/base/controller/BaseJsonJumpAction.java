package org.base.controller;

import org.springframework.ui.ModelMap;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class BaseJsonJumpAction {

	protected String jmodule;

	public ModelMap msg(String msg_text) {
		ModelMap m = new ModelMap();
		m.put("state", 0);
		m.put("msg", msg_text);
		return m;
	}

	public ModelMap jump(String url) {
		ModelMap m = new ModelMap();
		m.put("state", 1);
		m.put("url", url);
		return m;
	}

	public ModelMap jump(String url, String msg) {
		ModelMap m = new ModelMap();
		m.put("state", 1);
		m.put("url", url);
		m.put("msg", msg);
		return m;
	}

	public String ModuleUrl(String url) {
		return jmodule + url;
	}

	public String RedirectModuleUrl(String url) {
		return "redirect:" + jmodule + url;
	}

	public void setJmodule(String module) {
		// TODO Auto-generated method stub
		jmodule = module;
	}

	public JSONObject Success(JSONObject json) {
		JSONObject ret = new JSONObject();
		ret.put("code", 1);
		ret.put("data", json);
		return ret;
	}

	public JSONObject Success(String name, JSONArray jsonArray) {
		JSONObject data = new JSONObject();
		data.put(name, jsonArray);

		JSONObject ret = new JSONObject();
		ret.put("code", 1);
		ret.put("data", data);
		return ret;
	}

	public JSONObject Success(String str) {

		JSONObject ret = new JSONObject();
		ret.put("code", 1);
		ret.put("reason", str);
		return ret;
	}

	public JSONObject Fail(String str) {
		JSONObject ret = new JSONObject();
		ret.put("code", 0);
		ret.put("reason", str);
		return ret;
	}

	public JSONObject Success() {
		JSONObject ret = new JSONObject();
		ret.put("code", 1);
		return ret;
	}

	public JSONObject NotLogin() {
		JSONObject ret = new JSONObject();
		ret.put("code", 0);
		ret.put("reason", "用户未登录");
		return ret;
	}

	public JSONObject DataWrong() {
		return Fail("服务器数据异常");
	}

	public void L(String str) {
		System.out.println(str);
	}

}
