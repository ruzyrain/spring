package org.base.controller;

import net.sf.json.JSONObject;
import org.apache.log4j.Logger;
import org.base.entity.User;
import org.base.service.UserService;
import org.base.util.Utility;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by taoye on 16/1/27.
 */

@RestController
@RequestMapping("/user")
public class UserController extends BaseJsonJumpAction{

    Logger logger = Logger.getLogger(this.getClass());

    @Resource
    UserService userService;



    @RequestMapping("/login")
    public ModelAndView loginPage(HttpSession session){

        if (session.getAttribute("user") != null) return new ModelAndView("redirect:/home");

        return  new ModelAndView("user/login");
    }

    @RequestMapping("/edit")
    public ModelAndView editInfo(HttpSession session){

        User user = (User)session.getAttribute("user");

        ModelAndView mv = new ModelAndView("user/edit","user",user);

        return mv;
    }

    @RequestMapping("/login.json")
    public JSONObject loginJson(@RequestParam(value = "username", required = false) String userName,
                                @RequestParam(value = "password", required = false) String password,
                                HttpSession session){

        if (session.getAttribute("user") != null) return Fail("Already log in");

        User user = userService.getUserByNameAndPass(userName, password);
        if (user == null) return Fail("wrong password");

        session.setAttribute("user", user);

        return Success("Hello " + user.getUserName() + " !");
    }

    @RequestMapping("/regist.json")
    public JSONObject regist(@RequestParam(value = "username", required = false) String userName,
                             @RequestParam(value = "password", required = false) String password,
                             @RequestParam(value = "avatar", required = false) MultipartFile file,
                             HttpSession session) throws IOException {

        if (userService.getByUserName(userName)) return Fail("name already exist");


        User user = new User();
        user.setPassword(password);
        user.setUserName(userName);
        String path = "/img/default/avatar.jpg";
        if (file != null && !file.isEmpty()) {


            path = Utility.saveFile("avatar",file);


        }
        user.setImage(path);
        User result = userService.save(user);

        logger.info("user id is " + result.getId());
        session.setAttribute("user",result);

        return Success(result.getId().toString());
    }

    @RequestMapping("/editavatar.json")
    public JSONObject editAvatar(
            @RequestParam(value = "avatar", required = false) MultipartFile file,
            HttpSession session){

        if (file == null || file.isEmpty()) {
            return Fail("File is null");
        }

        User user = (User)session.getAttribute("user");

        //remove old & save new
        Utility.removeFile(user.getImage());

        String path = Utility.saveFile("avatar",file);

        user.setImage(path);
        User newUser = userService.save(user);
        session.setAttribute("user", newUser);
        JSONObject reJson = new JSONObject();
        reJson.put("user", newUser);
        return Success(reJson);
    }

    @RequestMapping("/info.json")
    public JSONObject info(HttpSession session){

        User user = (User)session.getAttribute("user");
        if (user == null) return Fail("not log in");

        JSONObject reJson = new JSONObject();
        reJson.put("user", user);
        return Success(reJson);
    }


    @RequestMapping("/logout.json")
    public ModelAndView logout(HttpSession session){

        session.removeAttribute("user");

        return new ModelAndView("user/login");
    }
}
