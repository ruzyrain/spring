package org.base.controller;

import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by taoye on 16/1/28.
 */

@Controller
public class ErrorHandlerController implements ErrorController{


    @Override
    public String getErrorPath() {
        return "/common/error";
    }

    @RequestMapping("/error")
    public String errorHandler(){
        return getErrorPath();
    }
}
