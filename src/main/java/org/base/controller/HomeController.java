package org.base.controller;

import net.sf.json.JSONObject;
import org.apache.log4j.Logger;
import org.base.entity.Article;
import org.base.entity.Comment;
import org.base.entity.User;
import org.base.service.ArticleService;
import org.base.service.CommentService;
import org.base.service.UserService;
import org.base.util.JsonTools;
import org.base.util.Utility;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by taoye on 16/1/27.
 */

@Controller
@RequestMapping("/home")
public class HomeController extends BaseJsonJumpAction{

    Logger logger = Logger.getLogger(this.getClass());

    @Resource
    UserService userService;

    @Resource
    ArticleService articleService;

    @Resource
    CommentService commentService;

    @RequestMapping("")
    public String pageLoad(ModelMap m , HttpSession session){

        User user = (User)session.getAttribute("user");
        m.put("user", user);

        return "home/home";
    }

    @RequestMapping("/article")
    public String articleInfo(
            @RequestParam(value = "articleid", required = false) Long articleId,
            ModelMap m , HttpSession session){

        User user = (User)session.getAttribute("user");
        Article article = articleService.getById(articleId);
        Page<Comment> commentPage = commentService.getAllByArticleAndPage(articleId,0);
        m.put("article", article);
        m.put("commentlist", commentPage.getContent());

        return "home/article";
    }

    @RequestMapping("/articlelist.json")
    public @ResponseBody JSONObject articleList(
            @RequestParam(value = "page", required = false) Integer page,
            HttpSession session){

        User user = (User)session.getAttribute("user");

        if (page == null || page < 1) page = 1;
        //用户从第一页开始，故应该减1
        --page;
        Page<Article> list = articleService.getAllByTimeAndPage(user.getId(), page);

        return Success("articlelist", JsonTools.articleList(list));

    }

    @RequestMapping("/articleadd.json")
    public @ResponseBody JSONObject articleAdd(
            @RequestParam(value = "title", required = false) String title,
            @RequestParam(value = "content", required = false) String content,
            @RequestParam(value = "image", required = false) MultipartFile file,HttpSession session
            ){

        User user = (User)session.getAttribute("user");
        Article article = new Article();
        article.setTitle(title);
        article.setContent(content);
        article.setTime(new Date());
        article.setUser(user);

        String path = "/img/default/avatar.jpg";
        if (file != null && !file.isEmpty()) {
            path = Utility.saveFile("article", file);
        }
        article.setImage(path);

        Article result = articleService.save(article);
        if (result == null) return Fail("失败");
        return Success();
    }


    @RequestMapping("/commentadd.json")
    public @ResponseBody JSONObject commentAdd(
            @RequestParam(value = "articleid", required = false) Long articleId,
            @RequestParam(value = "content", required = false) String content,
            HttpSession session
    ){

        User user = (User)session.getAttribute("user");
        Article article = articleService.getById(articleId);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:dd");
        Comment co = new Comment();
        co.setId(articleId + " - " + user.getUserName() + " - " + sdf.format(new Date()));
        co.setArticle(article);
        co.setUser(user);
        co.setContent(content);
        co.setImage("");
        co.setTime(new Date());

        commentService.save(co);
        return Success();
    }

    @RequestMapping("/commentlist.json")
    public @ResponseBody JSONObject commentList(
            @RequestParam(value = "articleid", required = false) Long articleId,
            @RequestParam(value = "page", required = false) Integer page){

        if (page == null || page < 1) page = 1;
        //用户从第一页开始，故应该减1
        --page;

        Page<Comment> list = commentService.getAllByArticleAndPage(articleId, page);

        return Success("commentlist", JsonTools.commentList(list));

    }

}
